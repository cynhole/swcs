include "cl_crosshair.lua"

include "shared.lua"

function SWEP:DrawWeaponSelection(x,y, w,h, alpha)
	if not self.SelectIcon then
		return end

	local Icon = self.SelectIcon
	if self.SelectIconAlt and self:GetWeaponMode() == Primary_Mode then
		Icon = self.SelectIconAlt
	end

	surface.SetMaterial(Icon)

	local col = surface.GetDrawColor()
	if col.r == 0 and col.g == 0 and col.b == 0 then
		col.r = 255
		col.g = 255
		col.b = 255
	end

	surface.SetDrawColor(col.r,col.g,col.b, alpha)

	local iDrawX = x + (w / 2)
	local iDrawY = y + (h / 2)

	local iMatWidth = Icon:Width() * 0.75
	local iMatHeight = Icon:Height() * 0.75

	local iHeight = ScreenScaleH(32)
	local flRatio = (iHeight / iMatHeight)

	local iWidth = iMatWidth * flRatio

	iDrawX = iDrawX - (iWidth / 2)
	iDrawY = iDrawY - (iHeight / 2)

	surface.DrawTexturedRect( iDrawX, iDrawY, iWidth, iHeight )
end

net.Receive("swcs_CallOnClients", function()
	local wep = net.ReadEntity()
	local func = net.ReadString()
	local args = net.ReadString()

	if not IsValid(wep) then return end

	if not wep.IsSWCSWeapon then
		ErrorNoHalt(Format("[swcs] Received CallOnClients for %s, but it's not an SWCS weapon!", tostring(wep)))
		return
	end

	if not wep[func] or not isfunction(wep[func]) then
		ErrorNoHalt(Format("[swcs] Received CallOnClients for %s, calling unknown function %q!", tostring(wep), func))
		return
	end

	wep[func](wep, args)
end)

SWEP.m_flStatTrakGlowMultiplierIdeal = 0
SWEP.m_flStatTrakGlowMultiplier = 0
function SWEP:UpdateStatTrakGlow()
	--approach the ideal in 2 seconds
	self.m_flStatTrakGlowMultiplier = swcs.Approach(self.m_flStatTrakGlowMultiplierIdeal, self.m_flStatTrakGlowMultiplier, FrameTime() * 0.5)
end

function SWEP:SetStatTrakGlowMultiplier(flNewIdealGlow)
	self.m_flStatTrakGlowMultiplierIdeal = flNewIdealGlow
end

function SWEP:GetStatTrakGlowMultiplier()
	return self.m_flStatTrakGlowMultiplier
end
