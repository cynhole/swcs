AddCSLuaFile()

-- econ as in skins and shit ;)

--[[
local ak_neon_rider = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/custom/ak_neon_rider",
	wearvalue = 1
})
local ak_uv = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/uvs/weapon_ak47",
	wearvalue = 1
})

local css_uv = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/uvs/weapon_knife_css",
	wearvalue = 1
})

local awp_cat = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/anodized_multi/workshop/awp_pawpaw",
	wearvalue = 1
})
local awp_medusa = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/antiqued/medusa_awp",
	wearvalue = 1
})

local deagle_etched = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/antiqued/etched_deagle",
	wearvalue = 1
})

local cz_etched = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/antiqued/etched_cz75",
	normalmap = "models/weapons/customization/paints/antiqued/etched_cz75_normal",
	wearvalue = 1
})

local bayonet_future = swcs.econ.GenerateEconTexture({
	basetexture = "models/weapons/customization/paints/gunsmith/bayonet_future_alt",
	normalmap = "models/weapons/customization/paints/gunsmith/bayonet_future_alt_normal",
	wearvalue = 1
})
]]

if CLIENT then
	function SWEP:ApplyWeaponSkin(vm, owner)
		local selfTable = self:GetTable()

		if not selfTable.m_clSkinTexture then
			--local want_skin = owner:GetPData("swcs_skin")
			--if want_skin == nil then return end
			--print(vm, selfTable.m_clSkinTexture, want_skin)
	
			--selfTable.m_clSkinTexture = "!" .. ak_neon_rider
		end
		if not selfTable.m_clSkinTexture then return end

		--vm:SetSubMaterial(0, selfTable.m_clSkinTexture)
	end

	function SWEP:RemoveWeaponSkin(vm, owner)
		vm:SetSubMaterial(0)
	end

	SWEP.m_viewmodelStatTrakAddon = NULL
	function SWEP:CreateViewmodelAttachments(vm, econItem)
		local selfTable = self:GetTable()
		local bFlip = selfTable.ViewModelFlip

		if econItem.bHasStatTrak then
			local statTrackClEnt = selfTable.m_viewmodelStatTrakAddon or NULL
			if not statTrackClEnt:IsValid() then
				statTrackClEnt = ClientsideModel(
					self:GetWeaponType() ~= "knife" and "models/weapons/csgo/stattrack.mdl" or "models/weapons/csgo/stattrack_cut.mdl",
					RENDERGROUP_VIEWMODEL)

				statTrackClEnt:SetParent(vm)
				statTrackClEnt:AddEffects(EF_NODRAW)
				statTrackClEnt:AddEffects(EF_BONEMERGE)
				statTrackClEnt:AddEffects(EF_BONEMERGE_FASTCULL)

				selfTable.m_viewmodelStatTrakAddon = statTrackClEnt
			else
				-- full update
				if statTrackClEnt:GetParent() ~= vm then
					statTrackClEnt:SetParent(vm)
				end

				local iBodygroup = statTrackClEnt:GetBodygroup(0)
				if iBodygroup ~= -1 then
					if iBodygroup == 0 and bFlip then
						statTrackClEnt:SetBodygroup(0, 1)
					elseif iBodygroup == 1 and not bFlip then
						statTrackClEnt:SetBodygroup(0, 0)
					end
				end
			end
		end

		-- nametag
		if econItem.strCustomName and #econItem.strCustomName > 0 then
			local uidClEnt = selfTable.m_viewmodelUidAddon or NULL
			if not uidClEnt:IsValid() then
				uidClEnt = ClientsideModel("models/weapons/csgo/uid.mdl", RENDERGROUP_VIEWMODEL)
				uidClEnt:SetParent(vm)
				uidClEnt:AddEffects(EF_NODRAW)
				uidClEnt:AddEffects(EF_BONEMERGE)
				uidClEnt:AddEffects(EF_BONEMERGE_FASTCULL)

				selfTable.m_viewmodelUidAddon = uidClEnt
			else
				if uidClEnt:GetParent() ~= vm then
					uidClEnt:SetParent(vm)
				end

				local iBodygroup = uidClEnt:GetBodygroup(0)
				if iBodygroup ~= -1 then
					if iBodygroup == 0 and bFlip then
						uidClEnt:SetBodygroup(0, 1)
					elseif iBodygroup == 1 and not bFlip then
						uidClEnt:SetBodygroup(0, 0)
					end
				end
			end
		end

		-- stickers?
	end

	function SWEP:RenderViewmodelAttachments(econItem)
		local selfTable = self:GetTable()
		local bViewmodelFlip = selfTable.ViewModelFlip

		if econItem.bHasStatTrak and selfTable.m_viewmodelStatTrakAddon then
			if bViewmodelFlip then
				render.CullMode(MATERIAL_CULLMODE_CW)
			end
			selfTable.m_viewmodelStatTrakAddon:DrawModel()
		end

		local bHasUID = econItem.strCustomName and #econItem.strCustomName > 0
		if bHasUID then
			if bViewmodelFlip then
				render.CullMode(MATERIAL_CULLMODE_CW)
			end
			selfTable.m_viewmodelUidAddon:DrawModel()
		end

		render.RenderFlashlights(function()
			if econItem.bHasStatTrak and selfTable.m_viewmodelStatTrakAddon then
				if bViewmodelFlip then
					render.CullMode(MATERIAL_CULLMODE_CW)
				end
				selfTable.m_viewmodelStatTrakAddon:DrawModel()

				if bHasUID then
					if bViewmodelFlip then
						render.CullMode(MATERIAL_CULLMODE_CW)
					end
					selfTable.m_viewmodelUidAddon:DrawModel()
				end

				render.CullMode(MATERIAL_CULLMODE_CCW)
			end
		end)

		render.CullMode(MATERIAL_CULLMODE_CCW)
	end
end

