AddCSLuaFile()

ENT.Base = "baseswcsgrenade_projectile"
ENT.m_flTimeToDetonate = 5
ENT.IsSWCSGrenade = true

DEFINE_BASECLASS(ENT.Base)

local GRENADE_MODEL = Model"models/Items/Flare.mdl"

function ENT:Create(pos, angs, vel, angvel, owner)
	self:SetPos(pos)
	self:SetAngles(angs)

	self:SetVelocity(vel)
	self:SetInitialVelocity(vel)

	if IsValid(owner) then
		self:SetThrower(owner)
		self:SetOwner(owner)
	end

	self:SetLocalAngularVelocity(angvel)
	self:SetFinalAngularVelocity(angvel)
	self:SetActualCollisionGroup(COLLISION_GROUP_PROJECTILE)

	return self
end

function ENT:Initialize()
	self:SetModel(GRENADE_MODEL)

	self:SetDetonateTimerLength( self.m_flTimeToDetonate )

	BaseClass.Initialize(self)
end

function ENT:Detonate()
	-- poof!!!

	if IsFirstTimePredicted() then

		-- predicted random bc im cool B)
		g_ursRandom:SetSeed(engine.TickCount())
		local col = Color(
			g_ursRandom:RandomInt(0, 255),
			g_ursRandom:RandomInt(0, 255),
			g_ursRandom:RandomInt(0, 255)
		)

		if CLIENT then
			local light = DynamicLight(self:EntIndex())
			light.pos = self:GetPos()
			light.r = col.r
			light.g = col.g
			light.b = col.b
			light.brightness = 5.0
			light.size = 200
			light.dietime = CurTime() + 0.1
			light.decay = 768
		end

		local effectdata = EffectData()
		effectdata:SetOrigin( self:GetPos() )
		effectdata:SetStart( Vector(col.r,col.g,col.b) )
		util.Effect( "balloon_pop", effectdata )
	end

	if SERVER then SafeRemoveEntity(self) end
end

function ENT:BounceSound()
	self:EmitSound("Flashbang.Bounce")
end

function ENT:AcceptInput(strInput, actor, caller, data)
	if string.lower(strInput) == "settimer" then
		self.m_flTimeToDetonate = tonumber(data)
		self:SetDetonateTimerLength(self.m_flTimeToDetonate)
	end
end
