# Counter-Strike: Global Offensive Weapons for Garry's Mod
Also known as Source Weapons CS:GO (SWCS)

[Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2193997180)

## Addons
- NOBLE Strike Weapons ([Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2924956809) | [Git](https://gitlab.com/cynhole/swcs_noble))
- [CS:GO Weapons - Extras](https://steamcommunity.com/sharedfiles/filedetails/?id=3010372400) (CS:GO alpha weapons)
- [GTA Vice City Weapons](https://gitlab.com/cynhole/swcs_vicecity) (indefinitely unfinished)

## Extras
- [CS:GO Danger Zone Entities](https://steamcommunity.com/sharedfiles/filedetails/?id=2964998341)
- [TTT Icon Generator](https://gitlab.com/cynhole/swcs_ttt_icongen)

## Credits
- Valve - CS:GO, original assets
- Tyler McVicker - Causing CS:GO's source code to get leaked
  - Nothing we've written is a 1:1 copy/port and theres code intentionally stubbed out, but it's been a good reference when needed
